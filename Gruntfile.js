module.exports = function(grunt){

	var app = "app";

	grunt.initConfig({
		pkg:grunt.file.readJSON('package.json'),
		clean: [
			'build',
			'.sass-cache'
		],
		jshint: {
			all: [app + '/javascript/**/*.js']
		},
        copy:{
            js:{
				expand: true,
				cwd: app + '/javascript/',
				src: '**/*.js',
				dest: 'build/javascript',
				extDot: 'last',
				flatten: true
            }
        },
		concat: {
			options: {
				separator: ';',
			},
			dist: {
				src: ['build/javascript/*.js'],
				dest: 'build/javascript/app-concat.js',
			}
		},
		uglify: {
			main: {
				files: {
					'build/javascript/app-concat.min.js': ['build/javascript/*.js']
				}
			}
		},
		jade: {
	        compile: {
	            options: {
	                pretty: true
	            },
	            files: [ {
					expand: true,
					cwd: app + "/jade",
					src: "**/*.jade",
					dest: "build/view",
					ext: ".html",
					extDot: "last"
	            } ]
	        }
	    },
		sass: {
			options: {
				sourceMap: false
			},
			dist: {
				files: {
					'build/style/main.css': app + '/sass/main.scss'
				}
			}
		},
		watch:{
			options:{
				livereload:true
			},
			javascript:{
				files:[app + '/javascript/**/*.js'],
				tasks:['jshint', 'copy', 'concat', 'uglify']
			},
			jade:{
				files:[app + '/jade/**/*.jade'],
				tasks:['jade']
			},
			sass:{
				files:[app + '/sass/**/*.scss'],
				tasks:['sass']
			}
		},
  		express:{
  			all:{
  				options:{
  					port:3000,
  					hostname:'localhost',
  					bases:['./build', './build/view'],
  					livereload:true
  				}
  			}
  		}
	});

	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-jade');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-express');

	grunt.registerTask('default',['jshint','clean', 'copy', 'concat', 'uglify', 'jade', 'sass', 'express','watch']);

};
